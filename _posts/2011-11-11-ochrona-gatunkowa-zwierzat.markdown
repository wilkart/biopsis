---
layout: post
title:  "Ochrona gatunkowa zwierząt"
date:   2011-11-11 15:39:00
categories: prawo
tags: arnold
---

{:.image .image-left}
![Czapla siwa](/images/67186339_ee4cfa0222_m.jpg "fot. Joaquim Coelho, flickr.com")

Po ponad trzech latach formalnego braku ochrony gatunkowej zwierząt w Polsce, 23 listopada 2011 r. wchodzi w życie nowe rozporządzenie Ministra Środowiska w [sprawie ochrony gatunkowej][og-rozp-2011] zwierząt. Czy dzięki tak długiemu procesowi przygotowywania powstał akt rzeczywiście dopracowany, zgodny ze współczesną wiedzą, prawem wspólnotowym i zasadami techniki prawodawczej? Komentarz tego aktu prawnego w [Magazynie Przyrodniczym SALAMANDRA][og-salamandra].

[og-rozp-2011]: http://isap.sejm.gov.pl/Download;jsessionid=7FFDDBDF2923C27E7F740B8AB9B695B7?id=WDU20112371419&type=2
[og-salamandra]: http://magazyn.salamandra.org.pl/m32a10.html
