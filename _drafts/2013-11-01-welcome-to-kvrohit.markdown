---
layout    : post
title     : "Welcome to kvrohit.github.io"
date      : 2013-11-01
excerpt   : Greetings, welcome to my blog!
categories:
- blog
---
Greetings, welcome to my blog! As you might have noticed, this blog is under construction and is still rough around the edges. Up until now, I maintained a blog over at tumblr, which, was pretty good but did not offer a lot of flexibility.

I am in the process of migrating all posts over to the new blog.

The blog itself is powered by *Jekyll* and hosted at *GitHub*. *Jekyll* offers full control over the content and the design and has some neat features such as: `Syntax highlighting`. Here's an example...

{% highlight ruby %}
def print_hi(name)
  puts "Hi, #{name}"
end
print_hi('Tom')
#=> prints 'Hi, Tom' to STDOUT.
{% endhighlight %}

If you are interested in using Jekyll yourself then, check out the [Jekyll docs][jekyll] for more info.

Cheers!

[jekyll-gh]: https://github.com/mojombo/jekyll
[jekyll]:    http://jekyllrb.com
