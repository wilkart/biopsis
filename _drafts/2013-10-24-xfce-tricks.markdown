---
layout    : post
title     : XFCE tricks
excerpt   : Binding hardware volume control to mixer
categories:
- blog
---
Binding hardware volume control to mixer

Go to `Settings > Keyboard > Shortcuts` and add the following:

{% highlight bash %}
amixer set Master 5%+
amixer set Master 5%-
amixer set Master toggle
{% endhighlight %}

Source: [http://schman.at/shortcuts-mit-xfce-setzen](http://schman.at/shortcuts-mit-xfce-setzen)
